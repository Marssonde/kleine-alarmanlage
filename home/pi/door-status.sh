#!/bin/bash

# variables
emailaddress=""
emailhost=""
smtpport=""
emailpassword=""
gpg_publickey=""
homepath="/home/pi"
logpath="$homepath/log"
# erste Ziffer 0=open;1=closed
# Magnetschalter offen oder zu (Tür auf oder zu)
doorstatus=$1
# unverschlüsselter Debug Info: 1. Ziffer = Übergabeparameter der gpio.py und 2. Ziffer = GPIO Pin 17 (Magnetschalter für die Tür) und zur Verschleierung ein paar Zufallszahlen hinten dran
number=$doorstatus$RANDOM
# Statustext für die Tür (offen oder zu)
status="$logpath/status.log"
#vorhandene alte Dateien löschen
if [ -f $status.asc ]
then
   rm -f $status.asc
fi
TEXTLEFT=""
#Statustext (Übergabeparameter von gpio.py in $1)
case "$1" in
        0) echo "door should be open" > $status
           TEXTLEFT="door_open"
           ;;
        1) echo "door should be closed" > $status
           TEXTLEFT="door_closed"
           ;;
        2) echo "Bewegung erkannt" > $status
            TEXTLEFT="detection_start"
            ;;
        3) echo "Bewegung beendet" > $status
            TEXTLEFT="detection_end"
            ;;
        4) echo "pause on" > $status
            TEXTLEFT="pause_on"
            ;;
        5) echo "pause off" > $status
            TEXTLEFT="pause_off"
            ;;
        6) echo "detection pause" > $status
            TEXTLEFT="motion_pause"
            ;;
        7) echo "detection start" > $status
           TEXTLEFT="motion_start"
            ;;
        8) echo "Shutdown_now" > $status
           TEXTLEFT="Shutdown_now"
            ;;
        *) echo "unknown param: "$1 > $status
           TEXTLEFT="unknown_event"
           ;;


esac
# curl -s -o /dev/null http://localhost:7999/0/config/set?text_left=$TEXTLEFT > /dev/null
# über motion einen Schnappschuss anlegen (Foto)
curl -s -o /dev/null http://localhost:7999/0/action/snapshot
# kurze Wartezeit einlegen, indem das Foto erstellt wird
sleep 0.5 # Waits 0.5 second.
# Der endgültige Statustext, der verschlüsselt versendet wird
tail -n 7 $logpath/gpio.log >> $status
#letztes Bild wird verschlüsselt
encimage=$homepath"/motion/Camera1/lastsnap.jpg"
#vorhandene alte Dateien löschen
if [ -f $encimage.asc ]
then
   rm -f $encimage.asc
fi
# Foto verschlüsseln
if [ -f $encimage ]
then
   gpg2 --encrypt -a --recipient $gpg_publickey $encimage >> $logpath/gpg2.log
fi
#Statustext verschlüsseln
   if [ -f $status ]
   then
      gpg2 --encrypt -a --recipient $gpg_publickey $status >> $logpath/gpg2.log
   fi
   # Senden des verschlüsselten Statustextes und des verschlüsselten Foto falls sie vorhanden sind
   # die folgenden Abfragen sind dazu gedacht, auch im Fehlerfall eine Email zu senden falls kein Foto gemacht wurde oder der Statustext nicht erstellt wurde
   if [ -f $status.asc ]
   then
      # Abfrage ob es ein verschlüsseltes Foto gibt oder nicht. Dann per Email versenden
      # Statustext verschlüsselt vorhanden
      if [ -f $encimage.asc ]
      then
        sendEmail -f $emailaddress -t $emailaddress -u Status -s $emailhost:$smtpport -m $number -a $status.asc $encimage.asc -xu $emailaddress -xp $emailpassword  >> $logpath/sendemail.log
      else
        sendEmail -f $emailaddress -t $emailaddress -u Status -s $emailhost:$smtpport -m $number -a $status.asc -xu $emailaddress -xp $emailpassword  >> $logpath/sendemail.log
      fi
   else
     # Abfrage ob es ein verschlüsseltes Foto gibt oder nicht. Dann per Email versenden
     # Statustext nicht vorhanden
     if [ -f $encimage.asc ]
     then
       sendEmail -f $emailaddress -t $emailaddress -u Status -s $emailhost:$smtpport -m $number -a $encimage.asc -xu $emailaddress -xp $emailpassword  >> $logpath/sendemail.log
     else
       sendEmail -f $emailaddress -t $emailaddress -u Status -s $emailhost:$smtpport -m $number -xu $emailaddress -xp $emailpassword  >> $logpath/sendemail.log
     fi
   fi
if [ "$1" == "4" ]
then
   sudo /etc/init.d/motion stop
fi

if [ "$1" == "5" ]
then
   sudo /etc/init.d/motion start
fi

if [ "$1" == "8" ]
then
   sudo shutdown -h now
fi
