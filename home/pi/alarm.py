#!/usr/bin/env python

# Public Domain

import sys
import time
import pigpio
import datetime
import subprocess

stopp = 0  # Zeitpunkt steigende Flanke 
start = 0  # Zeitpunkt fallende Flanke
delta = 0  # Zeitdifferenz zwischen start und stopp
GPIOreedcontact=17
GPIOButton=16
GPIOGreenLED=24
GPIORedLED=25
GLITCH=400
GPIOSirene=23
pauseflag=0
pi = pigpio.pi()       # pi accesses the local Pi's GPIO
bewegungsflag=0
if not pi.connected:
   exit(0)

pi.set_mode(GPIOreedcontact, pigpio.INPUT)
pi.set_glitch_filter(GPIOreedcontact, GLITCH)
pi.set_mode(GPIOButton, pigpio.INPUT)
pi.set_glitch_filter(GPIOButton, GLITCH)

# doorstatus = pi.read(GPIOreedcontact)

pi.set_mode(GPIOSirene, pigpio.OUTPUT)
pi.set_mode(GPIORedLED, pigpio.OUTPUT)
pi.set_mode(GPIOGreenLED, pigpio.OUTPUT)
pi.write(GPIOGreenLED, 0) # set green LED on (Power on)
pi.write(GPIORedLED, 0) # set red led on
time.sleep(2) # important for function!!!
pi.write(GPIORedLED, 1) # set red led off

def cbf1(gpio, level, tick):
   global start
   global stopp
   global delta
   global GPIOSirene
   global GPIORedLED
   global pauseflag
   # fallende Flanke, Startzeit speichern
   if level == 0:
      start = time.time()
      print("------------------------------")
      print(" ")
      if pauseflag==0:
          pi.write(GPIOSirene, 1) # set Sirene on
      else:
          pi.write(GPIOSirene, 0) # set Sirene off 
      pi.write(GPIORedLED, 0) # set red led on
      print("Door is open")
      print(time.strftime("%d.%m.%Y %H:%M:%S"))
      print("------------------------------")
      subprocess.call(["/home/pi/door-status.sh","0"])
   if level == 1:
      # steigende Flanke, Endezeit speichern
      stopp = time.time()
      delta = stopp - start       # Zeitdifferenz berechnen
      print("------------------------------")
      print(" ")
      pi.write(GPIOSirene, 0) # set Sirene off
      pi.write(GPIORedLED, 1) # set red led off
      print("Door is closed = %1.2f" % delta)
      print(time.strftime("%d.%m.%Y %H:%M:%S"))
      print("------------------------------")
      subprocess.call(["/home/pi/door-status.sh","1"])

def cbf2(gpio, level, tick):
      global start
      global stopp
      global delta
      global GPIORedLED
      global GPIOGreenLED
      global pauseflag
      if level==0:
         start = time.time()
         stopp=0
         delta=0
      if level==1:
         stopp = time.time()
         delta = stopp - start  # Zeitdifferenz berechnen
         if delta>3:
             pi.write(GPIOGreenLED, 0) # led on
             pi.write(GPIORedLED, 0) # led on
             time.sleep(0.5)
             pi.write(GPIOGreenLED, 1) # led off
             pi.write(GPIORedLED, 1) # led off
             time.sleep(0.5)
             pi.write(GPIOGreenLED, 0) # led on
             pi.write(GPIORedLED, 0) # led on
             time.sleep(0.5)
             pi.write(GPIOGreenLED, 1) # led off
             pi.write(GPIORedLED, 1) # led off
             time.sleep(0.5)
             pi.write(GPIOGreenLED, 0) # led on 
             pi.write(GPIORedLED, 0) # led on 
             subprocess.call(["/home/pi/door-status.sh","2"])
         else:
             pi.write(GPIOSirene, 0) # set Sirene off
             if pauseflag==0:
                pauseflag=1
                pi.write(GPIOGreenLED, 1) # led off
                subprocess.call(["/home/pi/door-status.sh","3"])
             else:
                pauseflag=0
                pi.write(GPIOGreenLED, 0) # led on
                subprocess.call(["/home/pi/door-status.sh","4"])
         start=0
         stopp=0
         delta=0


try:
   cb1 = pi.callback(GPIOreedcontact, pigpio.EITHER_EDGE, cbf1)
   cb2 = pi.callback(GPIOButton, pigpio.EITHER_EDGE, cbf2)

   # Endlosschleife, bis Strg-C gedrueckt wird
   while True:
     # nix Sinnvolles tun
     time.sleep(100)
except KeyboardInterrupt:
   print ("\nBye")
   cb1.cancel()
   cb2.cancel()
   exit()
