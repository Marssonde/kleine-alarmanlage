# Kleine Alarmanlage (in Arbeit)
Alarmanlage mit RaspberryPiZero und Reed Koontakt.


# (Die Links und der Anbieter dienen nur zur Orientierung)
# 1. Benötigte Hardware: 
Name  |  Link  |  Bild
----------  |  ----------  |  ---------- 
MicroSDHC | https://www.reichelt.de/microsdhc-speicherkarte-32gb-samsung-evo-plus-sams-mb-mc32ga-p207608.html?&trstct=lsbght_sldr::164977 |  ![MicroSDHC](https://cdn-reichelt.de/bilder/web/artikel_ws/E910/SAMSUNG_MB-MC32GAEU_01.jpg "MicroSDHC")
Raspberry Pi - Netzteil | https://www.reichelt.de/raspberry-pi-netzteil-5-v-2-5-a-micro-usb-schwarz-rasp-nt-25-sw-e-p240934.html?&trstct=lsbght_sldr::164977 |  ![Netzteil](https://cdn-reichelt.de/bilder/web/artikel_ws/D400/RASPNT25SWE.jpg "Netzteil")
Gehäuse für Schaltung | https://www.reichelt.de/index.html?ACTION=446;GROUPID=7723;SEARCH=geh%C3%A4use |    ![Gehäuse](https://cdn-reichelt.de/resize_150x150/web/artikel_ws/C700/ALUFORM.jpg "Gehäuse")
Kabel für Schaltung | https://www.reichelt.de/entwicklerboards-steckbrueckenkabel-20-pole-m-m-f-f-f-m-25-debo-kabelset-p161046.html?&trstct=pos_2 |    ![Kabel](https://cdn-reichelt.de/bilder/web/artikel_ws/A300/KABELFRRASPBERRY.jpg "Kabel")
1 bis 2 Platinen | https://www.reichelt.de/Streifenraster/2/index.html?ACTION=2&LA=2&GROUPID=7786 |    ![Platinen](https://cdn-reichelt.de/resize_150x150/web/artikel_ws/C900/H25PS160.jpg "Platinen")
***
# 2. Elektronische Bauteile:
ich persönlich habe gleich mehrere Bauteile gekauft, für den Fall das ein Bauteil verloren oder kaputt geht.

## LEDS:
Name  |  Link  |  Foto
